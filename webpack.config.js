const path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require('webpack');

module.exports = {
  entry: './app/entry.js',
  output: {
    filename: 'app.js',
    path: path.join(__dirname, 'public/assets')
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.css/,
        loader: ExtractTextPlugin.extract("style-loader", "css-loader")
      },
      {
        test: /\.(woff|eot|woff2|svg|ttf)/,
        loader: 'url'
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("main.css"),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    })
  ]
};
