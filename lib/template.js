'use strict';

const fs = require('fs');
const path = require('path');
const ejs = require('ejs');
const assign = require('object.assign');

module.exports = {
  render: (filePath, options, callback) => {
    let settings = options.settings,
        layoutPath = path.join(settings.views, settings.layout);

    fs.readFile(filePath, (err, content) => {
      if(err) { return callback(new Error(err)); }
      
      let rendered = ejs.render(content.toString(), options, { cache: true, filename: '.cached' });

      let renderedLayout = ejs.renderFile(layoutPath, assign({ 
        content: rendered
      }, options), callback);
    });
  }
};
