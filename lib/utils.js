'use strict';

import assign from 'object.assign';

function toQuery(obj, _options) {
  let qs = [];
  let defaultOptions = { delimiter: '|' };
  let options = assign({},
                       defaultOptions,
                       typeof _options === 'undefined' ? {} : _options);

  for(let key in obj) {
    let val = obj[key];
    let str = '';
    if(Array.isArray(val)) {
      if(val.length > 0) {
        str = key + '=' + val.join(options.delimiter);
      }
    }
    else {
      if(val) { str = key + '=' + val }
    }

    if(str) { qs.push(str.trim()); }
  }

  return qs.join('&');
}

function toUrl(url, obj, options) {
  return `${url}/?${toQuery(obj, options)}`;
};

export { toQuery, toUrl };
