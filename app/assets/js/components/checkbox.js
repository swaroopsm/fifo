import React from 'react';

export default class Checkbox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: false
    }
  }

  handleChange(e) {
    let checked = !this.state.checked;
    this.setState({ checked: checked })
    this.props.onChange(checked);
  }

  componentWillMount() {
    this.setState({ checked: this.props.checked })
  }

  componentWillReceiveProps(props) {
    this.setState({ checked: props.checked })
  }

  render() {
    return (
      <div className=''>
        <input id={ this.props.label }
               type='checkbox'
               onChange={ this.handleChange.bind(this) }
               checked={ this.state.checked }
               ref={ `checkbox-${this.props.label}` } />
               &nbsp;
        <label htmlFor={ this.props.label }>{ this.props.label }</label>
      </div>
    );
  }
}
