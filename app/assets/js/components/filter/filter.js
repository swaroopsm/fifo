import React from 'react';
import MultiSelectFilter from './multi-select/list';
import RangeFilter from './range';
import assign from 'object.assign';
import SelectedFilter from './selected';
import { toUrl  }from '../../../../../lib/utils';

export default class Filter extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      selected: {}
    }
  }

  componentWillMount() {
    let selected = this.initSelectFilter();
    this.setState({ selected });
  }

  initSelectFilter() {
    let selected = {};
    let data = __store__.selectedFilters;

    for(let key in this.props.filters) {
      let filter = this.props.filters[key];

      selected[key] = data[key];
    }

    return selected;
  }

  handleMultiSelectChange(key, value, isChecked) {
    let filter = this.state.selected[key];

    if(isChecked) {
      filter.push(value);
    }
    else {
      let index = filter.indexOf(value);
      if(index >= 0) {
        filter.splice(index, 1);
      }
    }

    this.setState({ selected: this.state.selected });
  }

  handleRangeChange(key, values) {
    this.state.selected[key] = values.join(',');
    this.setState({ selected: this.state.selected });
  }

  componentDidUpdate(props, state) {
    this.refineResults();
  }

  refineResults() {
    let url = toUrl('', assign({},
                                { category: __store__.currentCategory.name},
                                this.state.selected));

    // Fetching happens here
    // For now just over-writing the URL
    window.history.pushState(null, __store__.currentCategory.name, url);
  }

  removeSelectedFilter(key, value) {
    let filter = this.state.selected[key];
    if(Array.isArray(filter)) {
      filter.splice(filter.indexOf(value), 1);
    }
    else {
      this.state.selected[key] = '';
    }

    this.setState({ selected: this.state.selected });
  }

  renderSelectedFilters() {
    let items = [];
    const renderSelected = (key, value) => {
      let { pre_sym, post_sym } = this.props.filters[key];
      let label = [ pre_sym, value, post_sym ].join(' ');

      return <SelectedFilter label={ label }
                             key={ label }
                             onRemove={ this.removeSelectedFilter.bind(this, key, label) } />
    };

    for(let key in this.state.selected) {
      let filter = this.state.selected[key];
      if(Array.isArray(filter)) {
        if(filter.length > 0) {
          items.push(filter.map((f) => renderSelected(key, f)))
        }
      }
      else {
        if(filter) {
          items.push(renderSelected(key, filter));
        }
      }
    }

    return (
      <ul className='selected-filters'>
        { items }
      </ul>
    )
  }

  render() {
    let filters = [];

    for(let key in this.props.filters) {
      let filter = Object.assign({ }, this.props.filters[key]);
      let component;
      filter.onChange = this.handleMultiSelectChange.bind(this, key);

      if(filter.type === 'multiSelection') {
        component = <MultiSelectFilter {...filter}
                                       key={ key }
                                       selected={ this.state.selected[key] } />
      }
      if(filter.type === 'range') {
        component = <RangeFilter {...filter.items}
                                 label={ filter.label }
                                 key={ key }
                                 pre_sym={ filter.pre_sym }
                                 post_sym={ filter.post_sym }
                                 onChange={ this.handleRangeChange.bind(this, key) }
                                 values={ this.state.selected[key] } />
      }

      filters.push(component);
    }

    return (
      <div>
        <ul className='filter-list category-list nav nav-tabs'>
          { filters }
        </ul>
        { this.renderSelectedFilters() }
      </div>
    )
  }
}

Filter.propTypes = {
  filters: React.PropTypes.object.isRequired
}
