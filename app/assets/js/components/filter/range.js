'use strict';

import Slider from 'nouislider';
import React from 'react';
import FilterItem from './item';

let { Component, PropTypes } = React;

export default class RangeFilter extends Component {

  constructor(props) {
    super(props);

    this.state = {
      start: undefined,
      end: undefined
    };
  }

  setLabelForHandle(props) {
    var values = this.getStartValues(props);

    this.setState({
      start: values[0],
      end: values[1]
    });
  }

  componentWillMount() {
    this.setLabelForHandle(this.props);
  }

  componentWillReceiveProps(props) {
    if(this.props.values === props.values) { return; }

    this.setLabelForHandle(props);
    this.resetSlider(props);
  }

  componentDidMount() {
    let $el = this.refs.range;
    let values = this.getStartValues(this.props);
    let start, end;

    [ start, end ] = values;

    Slider.create($el, {
      start: values,
      connect: true,
      step: +this.props.step,
      range: {
        min: +this.props.min,
        max: +this.props.max
      }
    });

    $el.noUiSlider.on('update', (v) => {
      this.setState({
        start: +v[0],
        end: +v[1]
      });
    });

    $el.noUiSlider.on('change', (v) => {
      this.props.onChange(v.map((val) => +val));
    });
  }

  resetSlider(props) {
    let { values } = props;
    if(!values.trim()) {
      let $el = this.refs.range;
      $el.noUiSlider.set(this.getStartValues(props));
    }
  }

  componentWillUnmount() {
    let $el = this.refs.range;

    $el.noUiSlider.off('change');
    $el.noUiSlider.off('update');
  }

  getStartValues(props) {
    let { min, max } = props;
    let values;

    if(props.values) {
      values = props.values.trim().split(',');
    }
    else {
      values = [ min, max ];
    }

    return values.map((v) => +v);
  }

  getLabelValue(val, pre, post) {
    if(typeof val === 'undefined') { return; }

    return [ pre, val, post ].join(' ');
  }

  render() {
    let { props, state } = this;

    return (
      <FilterItem label={ props.label }>
        <div className='nested-category-list range-filter-wrapper'>
          <div className='range-filter' ref='range'></div>
          <span className='range-left-val'>{ this.getLabelValue(state.start, props.pre_sym, props.post_sym) }</span>
          <span className='range-right-val'>{ this.getLabelValue(state.end, props.pre_sym, props.post_sym) }</span>
        </div>
      </FilterItem>
    );
  }
}

RangeFilter.propTypes = {
  values: PropTypes.string,
  min: PropTypes.string.isRequired,
  max: PropTypes.string.isRequired,
  step: PropTypes.string.isRequired,

};
