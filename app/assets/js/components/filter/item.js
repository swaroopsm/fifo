import React from 'react';

export default class FilterItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showList: false
    }
  }

  render() {
    return (
      <li onMouseOver={ () => this.setState({ showList: true }) } onMouseLeave={ () => this.setState({ showList: false }) }>
        <a href='#'>{ this.props.label }</a>
        <div style={ { display: this.state.showList ? 'block' : 'none' }} className='nav nav-tabs'>
          { this.props.children }
        </div>
      </li>
    )
  }
}
