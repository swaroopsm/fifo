import React from 'react';

export default class SelectedFilter extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li>{ this.props.label } <span onClick={ this.props.onRemove }>x</span></li>
    )
  }
}
