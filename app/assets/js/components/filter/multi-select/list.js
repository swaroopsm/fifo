'use strict';

import React from 'react';
import Checkbox from '../../checkbox';
import FilterItem from '../item';

export default class MultiSelectFilter extends React.Component {
  constructor(props) {
    super(props);
  }

  handleChange(item, isChecked) {
    this.props.onChange(item, isChecked);
  }

  render() {
    let items = this.props.items.map((item, index) => 
      <li key={ index }>
        <Checkbox label={ item }
                  id={ `item-${index}` }
                  onChange={ this.handleChange.bind(this, item) }
                  checked={ this.props.selected.indexOf(item) >= 0 } />
      </li>
    );

    return (
      <FilterItem label={ this.props.label }>
        <ul className='nav nav-tabs nested-category-list'>{ items }</ul>
      </FilterItem>
    );
  }
}
