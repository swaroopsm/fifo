'use strict';

const React = require('react');
const Item = require('./item');
const assign = require('object.assign');
const classNames = require('classnames');

class List extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    let items = this.props.categories.map((item) => React.createElement(Item, assign({
      key: item.id + '-' + item.name
    }, item)));
    let className = classNames('nav nav-tabs category-list', this.props.className);

    return React.createElement('ul', { style: this.props.style, className }, items);
  }
}

module.exports = List
