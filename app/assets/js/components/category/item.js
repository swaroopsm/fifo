'use strict';

const React = require('react');

class Item extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      show: false
    }
  }

  toggleList(show) {
    this.setState({ show: show });
  }

  render() {
    let nestedCategory = this.props.nestedCategory;
    let children = null;
    let item = null;

    if(nestedCategory && nestedCategory.length > 0) {
      children = React.createFactory(require('./list'))({
        categories: nestedCategory,
        style: {
          display: this.state.show ? 'block' : 'none'
        },
        className: 'nested-category-list'
      });
    }

    item = React.createElement('a', {
      onMouseOver: this.toggleList.bind(this, true),
      onMouseLeave: this.toggleList.bind(this, false)
    }, this.props.name);
    return React.createElement('li', null, item, children);
  }
}

module.exports = Item;
