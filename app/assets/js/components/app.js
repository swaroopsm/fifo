import React from 'react';
import Filter from './filter/filter';

let { Component } = React;

'use strict';

export default class App extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Filter filters={ window.__store__.filters }
                category={ window.__store__.currentCategory } />
        <h3 className='text-center'>Products Show Up here</h3>
      </div>
    );
  }
}
