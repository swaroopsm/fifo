'use strict';

let request = require('request');

class Category {

  static url(_area) {
    let area = _area || 'bangalore';

    return `https://www.gozefo.com/${area}/category/get/all/nestCategories`;
  }

  static all(_options, callback) {
    let options = typeof _options === 'undefined' ? {} : _options,
        url = this.url(options.area);

    request({ url: url, json: true }, function(error, response, body) {
      callback(body);
    });
  }

}

module.exports = Category;
