// TODO
// Ideally this must be a relaion hooked up to a category
// As in `A category has many filters`
'use strict';

class Filter {

  static filterize(filters) {
    // TODO
    // Get all available filters for a given category and map values accordingly
    return {
      condition: filters['condition'] && filters['condition'].split('|') || [],
      Material: filters['Material'] && filters['Material'].split('|') || [],
      Type: filters['Type'] && filters['Type'].split('|') || [],
      discount: filters['discount'] || '',
      offerPrice: filters['offerPrice'] || ''
    }
  }

  static all() {
    // For now return a static list of filter applicable for a table category
    return JSON.parse('{"condition":{"type":"multiSelection","label":"Condition","pre_sym":"","post_sym":"","items":["Unboxed","Gently Used","Like New","Well Used"]},"Type":{"type":"multiSelection","label":"Type","pre_sym":"","post_sym":"","items":["Dining Chairs","Three Seater","Dressing Tables","Side Tables","Wardrobes","Stools","Two Seater","Benches","Others","Side Table","Console Tables","Study Tables","Coffee Tables","Dining Tables","4 Seater Sets","6 Seater Sets"]},"offerPrice":{"type":"range","label":"Price","pre_sym":"Rs.","post_sym":"","items":{"min":"1000","max":"50000","step":"1000"}},"discount":{"type":"range","label":"Discount","pre_sym":"","post_sym":"%","items":{"min":"0","max":"100","step":"5"}},"Material":{"type":"multiSelection","label":"Material","pre_sym":"","post_sym":"","items":["Stainless Steel and Glass"," Teak Wood","Particle Board","Leatherette Cushion","Engineered Wood","Rexine","Leather Cushion","Red Maradi","Plastic","Indonesian Teak","Acacia Wood","Mango Wood","Beech Wood","Teak","Glass","Iron","HDF","Plywood","MDF","Rubber Wood","Fabric Cushion","Metal","Leatherette","Sheesham","Blackboard","Bamboo Wood","Oak Wood","Fabric","Pine Wood","Synthetic Rattan","Rubber","Reclaimed Wood","Bamboo","Fiber","Stone","Teak Wood","Acacia","Fiber Glass","Veener","Plaster of paris","Granite"," Fabric"," Glass"," Iron","Tropical Fruit Wood","Marble"]}}')
  }
}

module.exports = Filter;
