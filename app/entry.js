'use strict';

import React from 'react';
import { Component } from 'react';
import ReactDOM from 'react-dom';
import CategoryList from './assets/js/components/category/list';
import 'bootstrap/dist/css/bootstrap.css';
import './assets/css/app.css';
import './assets/css/category.css';
import './assets/css/filter.css';
import 'nouislider/distribute/nouislider.min.css';
import App from './assets/js/components/app';

// Render categories menu
let $menu = document.getElementById('menu');
ReactDOM.render(<CategoryList categories={ __store__.categories }/>, $menu);

// Render filters
let $main = document.getElementById('main');
ReactDOM.render(<App />, $main);
