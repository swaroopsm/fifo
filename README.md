## FIFO

E-commerce like filtering using React. https://enigmatic-shore-79983.herokuapp.com/

### Running in dev-mode

You need node >= 5.10.0 and npm 3.

- `$ git checkout development`
- `$ npm instal`
- Run server `$ npm run start-dev`
- Run webpack `$ npm run webpack`
- Go to http://localhost:3000 in your browser

### Deploying to heroku
- `$ npn run deploy`
