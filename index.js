const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const template = require('./lib/template');
const Category = require('./app/models/category');
const Filter = require('./app/models/filter');
const CategoryList = require('./app/assets/js/components/category/list');
const React= require('react');
const ReactDOM = require('react-dom/server');

/** ==============================
 * App Settings
 */
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'app/views'));
app.set('layout', 'layouts/app.ejs');
app.set('name', 'Fifo');

// Custom template rendering engine
app.engine('ejs', template.render);

// Serve static assets
app.use(express.static(__dirname + '/public'));

/** ==============================
 * Routes
 */

// Helper functions
const getCategories = (area) => {
  var promise = new Promise((resolve, reject) => {
    Category.all({ area }, (data) => {
      resolve(data);
    });
  });

  return promise;
};

// Home Route
app.get('/', (req, res) => {
  getCategories('bangalore').then((categories) => {
    var filters = Filter.all();
    var str = ReactDOM.renderToString(React.createFactory(CategoryList)({ categories }));
    // Ideally this must come from a request param. Hard-coding it for now.
    var currentCategory = categories[2];
    var selectedFilters = Filter.filterize(req.query);

    res.render('home/index', { categories, filters, str, currentCategory, selectedFilters });
  });
});

// App Startup
app.listen(process.env.PORT, () => {
  console.log(`App running at: ${process.env.PORT}`)
});
